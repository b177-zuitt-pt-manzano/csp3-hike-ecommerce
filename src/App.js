import React, {useState, useEffect} from 'react'
import './App.css'
import 
  {BrowserRouter as Router, 
  Routes, 
  Route} from 'react-router-dom';
import UserContext from './UserContext';

import Navs from './components/Navbar/Navs'
import Home from './pages/Home'
import Login from './pages/Login'
import Register from './pages/Register';
import Products from './pages/Products';
import AddProduct from './pages/AddProduct';
import PageNotFound from './components/PageNotFound';
import SingleProduct from './pages/SingleProduct';
import MyOrder from './pages/MyOrder';
import FooterPage from './pages/Footer';
// import Highlights from './pages/Highlights';

function App() {

	const [user, setUser] = useState(
		{
			id: null,
			isAdmin: null
		}
	);

	const unsetUser = () => {
		localStorage.clear();
		setUser({
			id: null,
			isAdmin: null
		})
	}

	useEffect( () => {
		let token = localStorage.getItem('token');
		fetch('https://calm-garden-57016.herokuapp.com/api/users/profile', 
          {
            method: "GET",
            headers: {
              "Authorization": `Bearer ${token}`
            }
          })
          .then(result => result.json())
          .then(result => {
            

            if(typeof result._id !== "undefined")
            {
              setUser
              ({
                id: result._id, isAdmin: result.isAdmin
              })
            } 
            else 
            {
              setUser
              ({ 
                id: null, isAdmin: null
              })
            }
          })
        }, [])

  return (
      <UserContext.Provider value={{user, setUser, unsetUser}}> 
        <Router>
            <Navs/>
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/login" element={<Login />} />
              <Route path="/register" element={<Register />}/>
              <Route path="/products" element={<Products />}/>
              <Route path="/addProducts" element={<AddProduct />}/>
              <Route path="/products/:productId" element={<SingleProduct />}/>
              <Route path="/myOrder" element={<MyOrder />}/>
              <Route path='/#highlights' element={<Home />}/>
              <Route element={<PageNotFound />} />
            </Routes>
            <FooterPage/>
        </Router>
      </UserContext.Provider>
  );
}

export default App;
